#+TITLE: Eliminacioni zadatak, Grupa A
#+AUTHOR: Osnove Programiranja FTN, Novi Sad
#+DATE: 4. Jul, 2016
#+LANGUAGE: sr
#+LATEX_HEADER: \usepackage[english]{babel}
#+OPTIONS: toc:nil

* Zadatak
  U programskom jeziku Pyhton implementirati program koji služi za analizu
  aktivnosti korisnika na društvenim mrežama.\\
  Datoteka sa podacima je data u sledećem formatu:

  #+BEGIN_SRC shell
    korisničko_ime | broj_komentara | broj_reakcija

    Marko|15|100
    Pera|18|120
    Pera|13|120
    Nikola|16|130
    Pera|13|120
    Marko|112|13
  #+END_SRC

  Na početku programa korisnik unosi korisničko ime nakon čega se na ekranu
  ispisuje ukupan broj komentara korisnika.

  *Napomena* Jedan korisnik može imati više zapisa u datoteci

* Rešenje                                                          :noexport:
  #+BEGIN_SRC python
    class User:
        def __init__(self, u_name, comments, likes):
            self.u_name = u_name
            self.comments = comments
            self.likes = likes

    fl = [User(l.split("|")[0].strip(), l.split("|")[1].strip,
               l.split("|")[2].strip()) for l in open("social.data", "r")]

    uname = input("Enter username: ").strip()

    import functools
    print(functools.reduce(lambda x, y: x + int(y.comments()),
                           list(filter(lambda x: x.u_name == uname, fl)), 0))
  #+END_SRC
