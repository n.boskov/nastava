#+TITLE: Eliminacioni zadatak, Grupa B
#+AUTHOR: Osnove Programiranja FTN, Novi Sad
#+DATE: 4. Jul, 2016
#+LANGUAGE: sr
#+LATEX_HEADER: \usepackage[english]{babel}
#+OPTIONS: toc:nil

* Zadatak
  U programskom jeziku Pyhton implementirati program koji služi za pronalaženje
  letova dužih od zadate vrednosti.\\
  Datoteka sa podacima o letovima je data u sledećem formatu:

  #+BEGIN_SRC shell
    Beograd | Vasington | 7623
    Helsinki | Beograd | 2308
    Abu Dabi | Beograd | 3779
    Beograd | Cikago | 8033
  #+END_SRC

  Na početku programa korisnik unosi minimalnu razdaljinu nakon čega se na ekranu
  ispisuju svi letovi duži od toga jedan za drugim.

* Rešenje                                                          :noexport:
  #+BEGIN_SRC python
    class Flight:
        def __init__(self, source, destination, length):
            self.source = source
            self.destination = destination
            self.length = length

    fl = [Flight(l.split("|")[0].strip(), l.split("|")[1].strip(),
                 int(l.split("|")[2].strip())) for l in open("flights.data", "r")]

    lenth = int(input("Enter minimal distance: ").strip())

    for flight in fl:
        if flight.length >= lenth:
            print(flight.source, " to ", flight.destination, ": ",  flight.length)
  #+END_SRC
