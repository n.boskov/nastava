# class User:
#     def __init__(self, u_name, comments, likes):
#         self.u_name = u_name
#         self.comments = comments
#         self.likes = likes

# fl = [User(l.split("|")[0].strip(), int(l.split("|")[1].strip()),
#            int(l.split("|")[2].strip())) for l in open("social.data", "r")]

# uname = input("Enter username: ").strip()

# import functools
# print(functools.reduce(lambda x, y: x + y.comments,
#                        filter(lambda x: x.u_name == uname, fl), 0))

class Flight:
    def __init__(self, source, destination, length):
        self.source = source
        self.destination = destination
        self.length = length

fl = [Flight(l.split("|")[0].strip(), l.split("|")[1].strip(),
             int(l.split("|")[2].strip())) for l in open("flights.data", "r")]

lenth = int(input("Enter minimal distance: ").strip())

for flight in fl:
    if flight.length >= lenth:
        print(flight.source, " to ", flight.destination, ": ",  flight.length)
